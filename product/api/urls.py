from django.urls import path
from .views import (
    ProductListView,
    ProductDetailView,
    AddToCartView,
    OrderDetailView
)

app_name = 'product'

urlpatterns = [
    path('products/', ProductListView.as_view(), name='product-list'),
    path('products/<pk>/', ProductDetailView.as_view(), name='product-detail'),
    path('add-to-cart/', AddToCartView.as_view(), name='add-to-cart'),
    path('order-summary/', OrderDetailView.as_view(), name='order-summary')
]