from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from product.models import Product, OrderProduct, Order
from .serializers import ProductSerializer, OrderProductSerializer, OrderSerializer


class ProductListView(ListAPIView):
    permission_classes = (AllowAny, )
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class ProductDetailView(RetrieveAPIView):
    permission_classes = (AllowAny, )
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class ProductUpdateView(RetrieveUpdateAPIView):
    permission_classes = (AllowAny, )
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class AddToCartView(APIView):
    def post(self, request, *args, **kwargs):
        slug = request.data.get('slug', None)
        if slug is None:
            return Response({"message": "Invalid request"}, status=HTTP_400_BAD_REQUEST)
        product = get_object_or_404(Product, slug=slug)
        order_product_qs = OrderProduct.objects.filter(product=product, user=request.user, ordered=False)

        if order_product_qs.exists():
            order_product = order_product_qs.first()
            order_product.quantity += 1
            order_product.save()
        else:
            order_product = OrderProduct.objects.create(product=product, user=request.user, ordered=False)

        order_qs = Order.objects.filter(user=request.user, ordered=False)
        if order_qs.exists():
            order = order_qs[0]
            if not order.products.filter(product__id=order_product.id).exists():
                order.products.add(order_product)
            return Response(status=HTTP_200_OK)
        else:
            order = Order.objects.create(user=request.user)
            order.products.add(order_product)
            return Response(status=HTTP_200_OK)


class OrderDetailView(RetrieveAPIView):
    serializer_class = OrderSerializer
    permission_classes = (AllowAny, )

    def get_object(self):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            return order
        except ObjectDoesNotExist:
            raise Http404("You do not have an active order")