from product.models import Product, OrderProduct, Order
from rest_framework import serializers


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'id',
            'title',
            'price',
            'available_size',
            'slug',
            'description',
            'image'
        )


class OrderProductSerializer(serializers.ModelSerializer):
    product = serializers.SerializerMethodField()
    total_product_price = serializers.SerializerMethodField()

    class Meta:
        model = OrderProduct
        fields = (
            'id',
            'product',
            'total_product_price',
            'quantity'
        )

    def get_product(self, obj):
        return ProductSerializer(obj.product).data

    def get_total_product_price(self, obj):
        return obj.get_total_product_price()


class OrderSerializer(serializers.ModelSerializer):
    order_products = serializers.SerializerMethodField()
    total_price = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = (
            'id',
            'order_products',
            'total_price'
        )

    def get_order_products(self, obj):
        return OrderProductSerializer(obj.products.all(), many=True).data

    def get_total_price(self, obj):
        return obj.get_total_price()
